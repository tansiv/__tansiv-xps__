# tansiv xps

Requirements:
- Create a virtualenv
- install python-tansiv (most likely in dev mode => will make QEMU_IMAGE points to the right location) / otherwise adapt


Adapt:
  - QEMU_IMAGE
  - path to python interpreter (the one in the venv)


Launch:

- We launch in batch mode. The parameters file is read and `oar` will starts as many jobs as lines in this file.
- Here are the commands

```bash
oarsub --array-param-file nts_params.txt -S ./nts_launcher.py

oarsub --array-param-file ts_params.txt -S ./ts_launcher.py
```


## What's the `oarsub` command is doing ?

- The file `ts_params.txt` is read. 
- For each line a new job is created 
- The `ts_launcher.py` script is read to get the parameter of the job (`-S` option == scan the script)
- The `ts_launcher.py` is launched and is passed a line of the parameter file as arguments.
- At the end as many jobs as line in the parameter file will run (concurrently) and write their outputs in the `results/` directory.
