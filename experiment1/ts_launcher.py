#!/home/asrivastava/python-tansiv/venv/bin/python

#OAR -l nodes=1,walltime=01:00:00
#OAR -p cluster='paravance'

# Invocation
# directory platform deployment simgrid_args

from pathlib import Path
from subprocess import check_call
import sys
import os

from tansiv.undercloud import localhost
from tansiv.api import start_ts, flent, dump
from tansiv.constants import QEMU_IMAGE, DEFAULT_DOCKER_IMAGE

# give me the power
# this is required by the start_* function
check_call("sudo-g5k")

print(sys.argv)
_, directory, platform, deployment  = sys.argv

# fail fast if the directory exists
working_dir = Path(directory)
working_dir.mkdir(parents=True, exist_ok=True)
#if not os.path.exists('directory'):
   # os.makedirs('directory')
#out = os.path.isdir("working_dir")
#print(out)

(working_dir / "cmd").write_text(" ".join(sys.argv))
(working_dir / "deployment.xml").write_text(Path(deployment).read_text())
(working_dir / "platform.xml").write_text(Path(platform).read_text())


roles, provider = localhost()
tansiv_roles, _ = start_ts(roles, 
            platform,
            deployment,
            QEMU_IMAGE, 
            DEFAULT_DOCKER_IMAGE,
        
         )

for _ in range(10):
     flent(tansiv_roles, working_dir=working_dir, duration=60)

dump(roles, tansiv_roles, working_dir=working_dir)
