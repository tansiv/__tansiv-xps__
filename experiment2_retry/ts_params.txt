# Two latencies used in this experiment : 10ms (star_10.xml) and 100ms (star_100.xml)

#10ms latency 
ts_results/10_ms/02_vms inputs/star_10.xml inputs/deployment2.xml 
ts_results/10_ms/04_vms inputs/star_10.xml inputs/deployment4.xml 
ts_results/10_ms/08_vms inputs/star_10.xml inputs/deployment8.xml 
ts_results/10_ms/16_vms inputs/star_10.xml inputs/deployment16.xml 
ts_results/10_ms/32_vms inputs/star_10.xml inputs/deployment32.xml 
ts_results/10_ms/48_vms inputs/star_10.xml inputs/deployment48.xml 
ts_results/10_ms/64_vms inputs/star_10.xml inputs/deployment64.xml 

#100ms latency
ts_results/100_ms/02_vms inputs/star_100.xml inputs/deployment2.xml 
ts_results/100_ms/04_vms inputs/star_100.xml inputs/deployment4.xml 
ts_results/100_ms/08_vms inputs/star_100.xml inputs/deployment8.xml 
ts_results/100_ms/16_vms inputs/star_100.xml inputs/deployment16.xml 
ts_results/100_ms/32_vms inputs/star_100.xml inputs/deployment32.xml 
ts_results/100_ms/48_vms inputs/star_100.xml inputs/deployment48.xml 
ts_results/100_ms/64_vms inputs/star_100.xml inputs/deployment64.xml 
